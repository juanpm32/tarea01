package com.example.validaciondeedad

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {
            val edad = Integer.parseInt(edtEdad.text.toString())
            if ( edad < 18 ) { tvResultado.text = "Usted es menor de edad" }
            else { tvResultado.text = "Usted es mayor de edad" }

        }

    }
}